using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChessEngine.Web.Models.Api.Accounts;
using ChessEngine.Web.Models.Api.Games;
using ChessEngine.Web.Models.Context;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ChessEngine.Web.Controllers
{
    [Authorize]
    public class AccountsController : Controller
    {
        private readonly EngineContext _context;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;

        public AccountsController(EngineContext context, UserManager<User> userManager, SignInManager<User> signInManager)
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        [Route("~/api/authentication")]
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Authentication()
        {
            if (User.Identity.IsAuthenticated)
            {
                var userId = _userManager.GetUserId(User);
                var games = await _context.Games.Where(g => g.DarkUserId == userId ||
                                                            g.LightUserId == userId)
                                                .ToListAsync();

                var gameViewModels = new List<GameViewModel>();

                foreach (var game in games)
                {
                    var gameViewModel = new GameViewModel()
                    {
                        Name = game.Name,
                        StartDate = game.StartDate,
                        EndDate = game.EndDate
                    };

                    gameViewModel.LightUserName = game.LightUserId != null ? (await _userManager.FindByIdAsync(game.LightUserId)).UserName : "Unmatched";
                    gameViewModel.DarkUserName = game.DarkUserId != null ? (await _userManager.FindByIdAsync(game.DarkUserId)).UserName : "Unmatched";

                    gameViewModels.Add(gameViewModel);
                }

                return Ok(new UserViewModel()
                {
                    Id = userId,
                    UserName = User.Identity.Name,
                    Games = gameViewModels
                });
            }

            return Ok(new UserViewModel());
        }

        [Route("~/api/accounts/register")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Register([FromBody]RegisterRequest model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var user = new User()
            {
                Email = model.Email,
                UserName = model.Email
            };

            var identity = await _userManager.CreateAsync(user, model.Password);

            if (!identity.Succeeded)
                return BadRequest(identity.Errors);

            var result = await _signInManager.PasswordSignInAsync(user, model.Password, true, false);

            if (result.Succeeded)
                return Ok(model);

            return Unauthorized();
        }

        [Route("~/api/accounts/login")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromBody]LoginRequest model)
        {
            if (model.Email == "engine@engine.engine")
                return Unauthorized();

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var user = await _userManager.FindByEmailAsync(model.Email);

            if (user == null)
                return NotFound(model);

            var result = await _signInManager.PasswordSignInAsync(user, model.Password, model.RememberMe, false);

            if (result.Succeeded)
                return Ok(model);

            return Unauthorized();
        }

        [Route("~/api/accounts/logout")]
        [HttpPut]
        public async Task<IActionResult> Logoug()
        {
            await _signInManager.SignOutAsync();
            return Ok(new UserViewModel());
        }
    }
}