﻿using System;
using System.Linq;
using ChessEngine.Web.Models.Context;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace ChessEngine.Web
{
    public class Startup
    {
        public static string CHESS_ENGINE_USER = "engine@engine.engine";

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<EngineContext>(
                options => options.UseSqlite("filename=./chess.sqlite")
            );

            services.AddIdentity<User, IdentityRole>()
                .AddEntityFrameworkStores<EngineContext>()
                .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 7;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;

                options.Cookies.ApplicationCookie.ExpireTimeSpan = TimeSpan.FromDays(10);

                options.User.RequireUniqueEmail = true;
            });

            services.AddMvc();
        }

        public async void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseIdentity();

            app.UseStaticFiles();

            app.UseMvc();

            using (var context = app.ApplicationServices.GetRequiredService<EngineContext>())
            {
                context.Database.EnsureCreated();
                if (!context.Users.Any(u => u.UserName == Startup.CHESS_ENGINE_USER)) 
                {
                    var user = new User()
                    {
                        Email = "engine@engine.engine",
                        UserName = Startup.CHESS_ENGINE_USER
                    };

                    using (var userManager = app.ApplicationServices.GetRequiredService<UserManager<User>>()) 
                    {
                        await userManager.CreateAsync(user, "---CHESS_ENGINE---");
                    }
                }
            }
        }
    }
}
