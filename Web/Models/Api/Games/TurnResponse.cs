namespace ChessEngine.Web.Models.Api.Games
{
    public struct TurnResponse
    {
        public const string CONFIRM = "CONFIRM";
        public const string DENY = "DENY";
        public const string GAMEOVER = "GAMEOVER";
    }
}