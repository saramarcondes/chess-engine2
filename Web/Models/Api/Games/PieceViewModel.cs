namespace ChessEngine.Web.Models.Api.Games
{
    public class PieceViewModel
    {
        public string Type { get; set; }
        public string Color { get; set; }
        public int File { get; set; }
        public int Rank { get; set; }
    }
}