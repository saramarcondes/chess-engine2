namespace ChessEngine.Web.Models.Api.Games
{
    public class MoveViewModel
    {
        public int FromFile { get; set; }
        public int FromRank { get; set; }
        public int ToFile { get; set; }
        public int ToRank { get; set; }
    }
}