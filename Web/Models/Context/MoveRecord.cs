namespace ChessEngine.Web.Models.Context
{
    public class MoveRecord
    {
        public int Id { get; set; }
        public int GameId { get; set; }
        public int FromFile { get; set; }
        public int FromRank { get; set; }
        public int ToFile { get; set; }
        public int ToRank { get; set; }

        public Game Game { get; set; }
    }
}