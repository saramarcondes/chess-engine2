using System.Collections.Generic;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace ChessEngine.Web.Models.Context
{
    public class User : IdentityUser
    {
        public ICollection<WinRecord> Wins { get; set; }
        public ICollection<Game> LightGames { get; set; }
        public ICollection<Game> DarkGames { get; set; }
    }
}