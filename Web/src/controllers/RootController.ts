import { IStateService } from 'angular-ui-router';

import { merge } from 'angular';

import AuthenticationService from '../services/AuthenticationService';
import AuthenticationValue from '../services/AuthenticationValue';
import LoginRequest from '../models/accounts/LoginRequest';
import RegisterRequest from '../models/accounts/RegisterRequest';

import { GAMES_ROUTE } from '../models/routes/browse/Games';
import { HOME_ROUTE } from '../models/routes/browse/Home';

export default class RootController {
    static $inject = ['$state', 'AuthenticationService', 'AuthenticationValue'];
    private state: IStateService;
    private authService: AuthenticationService;
    public authValue: AuthenticationValue;
    public errors: {}[] = [];

    constructor(
        private $state: IStateService,
        private AuthenticationService: AuthenticationService,
        public AuthenticationValue: AuthenticationValue
    ) {
        this.state = $state;
        this.authService = AuthenticationService;
        this.authValue = AuthenticationValue;
    }

    public loggedIn(): boolean {
        return this.authValue.id !== null;
    }

    public login(loginRequest: LoginRequest): void {
        this.authService.login(loginRequest).then(
            (res) => {
                this.authService.status().then(
                    (res) => {
                        merge(this.authValue, res.data);
                        this.state.go(GAMES_ROUTE);
                    },
                    (err) => {
                        console.error(err);
                    }
                );
            },
            (err) => {
                console.error(err);
            }
        );
    }

    public register(registerRequest: RegisterRequest) {
        this.authService.register(registerRequest).then(
            (res) => {
                this.authService.status().then(
                    (res) => {
                        merge(this.authValue, res.data);
                        this.state.go(GAMES_ROUTE);
                    },
                    (err) => {
                        console.error(err);
                    }
                );
            },
            (err) => {
                console.error(err);
            }
        );
    }

    public logout(): void {
        this.authService.logout().then(
            (res) => {
                merge(this.authValue, res.data);
                this.state.go(HOME_ROUTE);
            },
            (err) => {
                merge(this.authValue, new AuthenticationValue());
                console.error(err);
            }
        );
    }
}