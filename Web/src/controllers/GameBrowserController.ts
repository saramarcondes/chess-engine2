import { merge } from 'angular';

import AuthenticationValue from '../services/AuthenticationValue';
import DateStringFactory from '../services/DateStringFactory';
import GameService from '../services/GameService';
import Game from '../models/games/Game';

export default class GameBrowserController {
    static $inject = ['$state', 'GameService', 'AuthenticationValue', 'DateStringFactory'];
    private state: angular.ui.IStateService;
    private gameService: GameService;
    private authValue: AuthenticationValue;
    private dateStringFactory: DateStringFactory;
    public games(): Game[] { return this.authValue.games; };
    public unmatched: Game[] = [];

    constructor(
        private $state: angular.ui.IStateService,
        private GameService: GameService,
        private AuthenticationValue: AuthenticationValue,
        private DateStringFactory: DateStringFactory
    ) {
        this.state = $state;
        this.gameService = GameService;
        this.authValue = AuthenticationValue;
        this.dateStringFactory = DateStringFactory;

        this.resolveUnmatched();
    }

    private resolveUnmatched(): void {
        this.gameService.unmatched().then(
            (res) => {
                this.unmatched = res.data.map(
                    (obj: Game, i: number, arr: Game[]) => {
                        let game: Game = new Game();
                        merge(game, obj);
                        game.startDate = this.dateStringFactory.create(game.startDate);
                        return game;
                    }
                );
            },
            (err) => {
                console.error(err);
            }
        );
    }

    public join(name: string): void {
        this.gameService.join(name).then(
            (res) => {
                console.log(res.data);
            },
            (err) => {
                console.error(err);
            }
        );
    }

    public newGame(name: string): void {
        this.gameService.newGame(name).then(
            (res) => {
                console.log(res.data);
            },
            (err) => {
                console.error(err);
            }
        );
    }
}