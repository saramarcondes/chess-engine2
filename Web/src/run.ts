import { 
    IStateService,
    IUrlRouterProvider
} from 'angular-ui-router';

export default (
    $rootScope: ng.IRootScopeService,
    $urlRouter: IUrlRouterProvider,
    $state: IStateService
) => {
    $rootScope.$on('$stateChangeError', console.log.bind(console));
};