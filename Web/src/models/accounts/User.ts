import Game from '../games/Game';

interface User {
    id: string;
    userName: string;
    games: Game[];
}

export default User;