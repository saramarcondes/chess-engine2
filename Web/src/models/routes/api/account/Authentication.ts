import User from '../../../accounts/User';

export interface AuthenticationPromise extends ng.IHttpPromise<User> { }

export const AUTHENTICATION_ROUTE = '/api/authentication';
