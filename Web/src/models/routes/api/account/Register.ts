import RegisterRequest from '../../../accounts/RegisterRequest';

export interface RegisterPromise extends ng.IHttpPromise<RegisterRequest> { }

export const REGISTER_ROUTE = '/api/accounts/register';
