import WinRecord from '../../../games/WinRecord';

export interface WinRecordPromise extends ng.IHttpPromise<WinRecord> { }

export const WIN_RECORD_ROUTE = (name: string) => {
    return `/api/games/${name}/winner`;
}
