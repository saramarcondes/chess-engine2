import Move from '../../../games/Move';

export interface MovesPromise extends ng.IHttpPromise<Move[]> { }

export const MOVES_ROUTE = (name: string) => {
    return `/api/games/${name}/moves`;
};