import { ROOT_ROUTE } from './Root';

export class LoginRoute implements angular.ui.IState {
    public url: string = '/login';
    public template: string = require('./templates/login.html');
}

export const LOGIN_ROUTE = `${ROOT_ROUTE}.LOGIN`;