import { ROOT_ROUTE } from './Root';

export class HomeRoute implements angular.ui.IState {
    public url: string = '/';
    public template: string = require('./templates/home.html');
}

export const HOME_ROUTE = `${ROOT_ROUTE}.HOME`;
