import { ROOT_ROUTE } from './Root';

import GameService from '../../../services/GameService';

import { BoardPromise } from '../api/game/Board';

export interface IGameRouteParams extends angular.ui.IStateParamsService {
    name: string;
}

export class GameRoute implements angular.ui.IState {
    public url: string = '/game/:name';
    public controller: string = 'GameController';
    public controllerAs: string = 'gc';
    public template: string = require('./templates/game.html');
    public resolve: {} = {
        BoardPromise: this.BoardPromise()
    };

    private BoardPromise(): any[] {
        return [
            '$stateParams',
            'GameService',
            this.resolveBoard
        ];
    }

    private resolveBoard(
        $stateParams: IGameRouteParams,
        GameService: GameService
    ): BoardPromise {
        let promise = GameService.board($stateParams.name);
        promise.then(
            (res) => {
                return res.data;
            },
            (res) => {
                console.error(res.data);
            }
        );
        return promise;
    }
}

export const GAME_ROUTE = `${ROOT_ROUTE}.GAME`;