export default class WinRecord {
    gameName: string;
    userName: string;
    startDate: string;
    endDate: string;
}
