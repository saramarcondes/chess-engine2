export default class Piece {
    type: string;
    color: string;
    file: number;
    rank: number;
}
