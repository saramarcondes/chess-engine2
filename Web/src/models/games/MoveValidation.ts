import Move from './Move';
import WinRecord from './WinRecord';

export default class MoveValidation {
    message: string;
    move: Move;
    winRecord: WinRecord;
}
