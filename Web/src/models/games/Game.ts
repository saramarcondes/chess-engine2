export default class Game {
    name: string;
    lightUserName: string;
    darkUserName: string;
    winnerUserName: string;
    startDate: string;
    endDate: string;
}
