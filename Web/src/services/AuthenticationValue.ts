import User from '../models/accounts/User';
import Game from '../models/games/Game';

export default class Authentication implements User {
    public id: string;
    public userName: string;
    public games: Array<Game>;

    constructor() {
        this.id = null;
        this.userName = null;
        this.games = null;
    }
}