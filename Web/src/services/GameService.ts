import Move from '../models/games/Move';

import {
    GamePromise,
    GAME_ROUTE
} from '../models/routes/api/game/Game';
import {
    TurnPromise,
    TURN_ROUTE
} from '../models/routes/api/game/Turn';
import {
    BoardPromise,
    BOARD_ROUTE
} from '../models/routes/api/game/Board';
import {
    HistoryPromise,
    HISTORY_ROUTE
} from '../models/routes/api/game/History';
import {
    MovesPromise,
    MOVES_ROUTE
} from '../models/routes/api/game/Moves';
import {
    UnmatchedPromise,
    UNMATCHED_ROUTE
} from '../models/routes/api/game/Unmatched';
import {
    MoveValidationPromise,
    MOVE_VALIDATION_ROUTE
} from '../models/routes/api/game/MoveValidation';
import {
    WinRecordPromise,
    WIN_RECORD_ROUTE
} from '../models/routes/api/game/WinRecord';

export default class GameService {
    static $inject = ['$http'];
    private http: ng.IHttpService;

    constructor(
        private $http: ng.IHttpService
    ) {
        this.http = $http;
    }

    public game(name: string): GamePromise {
        return this.http.get(GAME_ROUTE(name));
    }

    public turn(name: string): TurnPromise {
        return this.http.get(TURN_ROUTE(name));
    }

    public board(name: string): BoardPromise {
        return this.http.get(BOARD_ROUTE(name));
    }

    public history(name: string): HistoryPromise {
        return this.http.get(HISTORY_ROUTE(name));
    }

    public moves(name: string): MovesPromise {
        return this.http.get(MOVES_ROUTE(name));
    }

    public unmatched(): UnmatchedPromise {
        return this.http.get(UNMATCHED_ROUTE);
    }

    public winner(name: string): WinRecordPromise {
        return this.http.get(WIN_RECORD_ROUTE(name));
    }

    public newGame(name: string): GamePromise {
        return this.http.post(GAME_ROUTE(name), null);
    }

    public makeMove(name: string, move: Move): MoveValidationPromise {
        return this.http.post(MOVE_VALIDATION_ROUTE(name), move);
    }

    public join(name: string): GamePromise {
        return this.http.put(GAME_ROUTE(name), null);
    }
}
