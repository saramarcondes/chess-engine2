import { module } from 'angular';

import config from './config';
import run from './run';

import RootController from './controllers/RootController';
import GameBrowserController from './controllers/GameBrowserController';
import GameController from './controllers/GameController';

import AuthenticationService from './services/AuthenticationService';
import GameService from './services/GameService';

import PieceFactory from './services/PieceFactory';
import DateStringFactory from './services/DateStringFactory';

import AuthenticationValue from './services/AuthenticationValue';

import PieceDirective from './directives/cePiece';
import CellDirective from './directives/ceCell';
import AudioToggleDirective from './directives/ceAudioToggle';
import HistoryToggleDirective from './directives/ceHistoryToggle';
import CurrentTurnDirective from './directives/ceCurrentTurn';

const app = module('App', [
    require('angular-ui-router')
])
    .config(config)
    .run(run)

    .controller('RootController', RootController)
    .controller('GameBrowserController', GameBrowserController)
    .controller('GameController', GameController)
    
    .service('AuthenticationService', AuthenticationService)
    .service('GameService', GameService)
    
    .service('PieceFactory', PieceFactory)
    .service('DateStringFactory', DateStringFactory)
    
    .value('AuthenticationValue', new AuthenticationValue())

    .directive('cePiece', PieceDirective)
    .directive('ceCell', CellDirective)
    .directive('ceAudioToggle', AudioToggleDirective)
    .directive('ceHistoryToggle', HistoryToggleDirective)
    .directive('ceCurrentTurn', CurrentTurnDirective);