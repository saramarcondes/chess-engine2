import GameController from '../controllers/GameController';
import Piece from '../models/games/Piece';

import { TURN_RESPONSE } from '../models/routes/api/game/Turn';

interface IPieceAttrs extends ng.IAttributes {
    file: string;
    rank: string;
    type: string;
    color: string;
    draggable: boolean;
}

interface IPieceScope extends ng.IScope {
    gc: GameController;
}

class Link {
    private file: number;
    private rank: number;
    private me: Piece;

    constructor(
        private scope: IPieceScope,
        private element: ng.IAugmentedJQuery,
        private attrs: IPieceAttrs
    ) {
        this.file = parseInt(attrs.file);
        this.rank = parseInt(attrs.rank);
        this.me = this.findMe();

        this.registerDragStart();
        this.registerMouseEnter();
        this.registerMouseLeave();
        this.registerWatchTakable();
    }

    private findMe(): Piece {
        let found = this.scope.gc.pieces.filter(
            (obj, i, arr) => {
                if (obj.file === this.file && obj.rank === this.rank)
                    return obj;
            }
        );

        if (found.length === 1)
            return found[0];
        else
            throw new Error("Something went wrong!!");
    }

    private registerWatchTakable(): void {
        this.scope.$watch('gc.hovered',
            (newValue: Piece, oldValue, scope) => {
                this.element.removeClass('takable');
                if (this.scope.gc.currentTurn !== TURN_RESPONSE.CONFIRM ||
                    this.scope.gc.historyToggle)
                    return;
                
                if (this.scope.gc.isDropAllowed(this.file, this.rank, false))
                    this.element.addClass('takable');   
            }
        );
    }

    private registerDragStart(): void {
        this.element.on('dragstart',
            (evt) => {
                this.scope.gc.draggedElement = this.element;
                this.scope.gc.dragged = this.me;
            }
        );
    }

    private registerMouseEnter(): void {
        this.element.on('mouseenter',
            (evt) => {
                if (this.scope.gc.currentTurn !== TURN_RESPONSE.CONFIRM ||
                    this.scope.gc.historyToggle)
                    return;
                
                this.scope.gc.hovered = this.me;
                this.scope.$apply();
            }
        );
    }

    private registerMouseLeave(): void {
        this.element.on('mouseleave',
            (evt) => {
                if (this.scope.gc.hovered === this.me) {
                    this.scope.gc.hovered = null;
                    this.scope.$apply();
                }
            }
        )
    }
}

export default function PieceDirective(): ng.IDirective {
    return {
        link: (scope: IPieceScope, element: ng.IAugmentedJQuery, attrs: IPieceAttrs) => new Link(scope, element, attrs)
    }
}