﻿using Engine.Models;
using System;

namespace Engine
{
    public class PieceFactory
    {
        public static Piece Create(char pieceCharacter, int file, int rank)
        {
            if (file < 0)
            {
                throw new ArgumentOutOfRangeException("file", "Must be greater than or equal to 0. ");
            }
            if (file > 7)
            {
                throw new ArgumentOutOfRangeException("file", "Must be less than or equal to 7. ");
            }
            if (rank < 0)
            {
                throw new ArgumentOutOfRangeException("rank", "Must be greater than or equal to 0. ");
            }
            if (rank > 7)
            {
                throw new ArgumentOutOfRangeException("rank", "Must be less than or equal to 7. ");
            }

            PieceColor color;
            if (pieceCharacter > 64 && pieceCharacter < 91)
            {
                color = PieceColor.Light;
            }
            else if (pieceCharacter > 96 && pieceCharacter < 123)
            {
                color = PieceColor.Dark;
            }
            else
            {
                throw new ArgumentException("Must be lowercase or uppercase a-z. ", "pieceCharacter");
            }

            switch (pieceCharacter)
            {
                case 'r':
                case 'R':
                    return new Rook(file, rank, color);
                case 'n':
                case 'N':
                    return new Night(file, rank, color);
                case 'b':
                case 'B':
                    return new Bishop(file, rank, color);
                case 'k':
                case 'K':
                    return new King(file, rank, color);
                case 'q':
                case 'Q':
                    return new Queen(file, rank, color);
                case 'p':
                case 'P':
                    return new Pawn(file, rank, color);
                default:
                    throw new FormatException("Invalid piece character format " + pieceCharacter + ". ");
            }
        }
    }
}
