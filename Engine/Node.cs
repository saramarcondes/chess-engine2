﻿using Engine.Models;
using System.Collections.Generic;

namespace Engine
{
    public class Node
    {
        private Move move;

        public Board Board { get; set; }
        public int Turn { get; set; }
        public MoveRecord ThisMove { get; set; }
        public List<Move> Moves { get; set; }
        public Node[] Next { get; set; }
        public int Points { get; set; }
        public int Depth { get; set; }
        public List<Node> Parents { get; set; } = new List<Node>();

        public Node(Board board, List<Move> moves, int turn, int points)
        {
            AutoMapper.Mapper.Initialize(c => c.CreateMap<Board, Board>());
            Board = AutoMapper.Mapper.Map<Board, Board>(board);
            Moves = moves;
            AutoMapper.Mapper.Initialize(c => c.CreateMap<Move, Move>());
            this.move = moves[moves.Count - 1];
            ThisMove = new MoveRecord()
            {
                FromFile = move.Piece.X,
                FromRank = move.Piece.Y,
                ToFile = move.ToX,
                ToRank = move.ToY
            };
            Turn = turn;
            Points = points;
        }

        public int CalculatePoints()
        {
            if (Board.Cells[ThisMove.ToFile, ThisMove.ToRank].Piece != null)
            {
                var piece = Board.Cells[ThisMove.ToFile, ThisMove.ToRank].Piece;
                
                switch (piece.Type)
                {
                    case PieceType.Pawn:
                        Points += 1;
                        break;
                    case PieceType.Night:
                    case PieceType.Bishop:
                        Points += 3;
                        break;
                    case PieceType.Rook:
                        Points += 5;
                        break;
                    case PieceType.Queen:
                        Points += 9;
                        break;
                    case PieceType.King:
                        Points = -1;
                        break;
                }
            }

            return Points;
        }

        public void BranchNode(int currentDepth, int finalDepth)
        {
            Depth = currentDepth;

            PieceColor color;
            if (Turn % 2 == 1)
            {
                color = PieceColor.Light;
            }
            else
            {
                color = PieceColor.Dark;
            }

            Board.MakeMove(this.move);

            if (currentDepth < finalDepth)
            {
                var allMoves = Board.AllMoves(color);
                Next = new Node[allMoves.Count];
            
                for (int i = 0; i < Next.Length; i++)
                {
                    var points = Points;
                    var moves = new List<Move>();
                    moves.AddRange(Moves);
                    moves.Add(allMoves[i]);
                    var node = Next[i] = new Node(Board, moves, Turn + 1, points);
                    if (node.CalculatePoints() != -1)
                    {
                        node.Parents.AddRange(this.Parents);
                        node.Parents.Add(this);
                        node.BranchNode(currentDepth + 1, finalDepth);
                    }
                }
            }
        }
    }
}
