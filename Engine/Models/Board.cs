﻿using System.Collections.Generic;
using System.Linq;

namespace Engine.Models
{
    public class Board
    {
        public Cell[,] Cells { get; set; }

        public Board()
        {
            Cells = new Cell[8, 8];

            for (int x = 0; x < 8; x++)
            {
                for (int y = 0; y < 8; y++)
                {
                    Cells[x, y] = new Cell();
                }
            }
        }

        public static Board Build(ICollection<MoveRecord> moves)
        {
            var board = new Board();
            board.Initialize();

            moves.OrderBy(m => m.Id);

            foreach (var move in moves)
            {
                int FromX = move.FromFile;
                int FromY = move.FromRank;
                int ToX = move.ToFile;
                int ToY = move.ToRank;

                var piece = board.Cells[FromX, FromY].Piece;

                piece.MoveTo(ToX, ToY);
                board.Cells[ToX, ToY].Piece = piece;
                board.Cells[FromX, FromY].Piece = null;
            }

            return board;
        }

        public void Initialize()
        {
            for (int x = 0; x < 8; x++)
            {
                Cells[x, 1].Piece = new Pawn(x, 1, PieceColor.Dark);
                Cells[x, 6].Piece = new Pawn(x, 6, PieceColor.Light);
            }

            Cells[0, 0].Piece = new Rook(0, 0, PieceColor.Dark);
            Cells[1, 0].Piece = new Night(1, 0, PieceColor.Dark);
            Cells[2, 0].Piece = new Bishop(2, 0, PieceColor.Dark);
            Cells[3, 0].Piece = new Queen(3, 0, PieceColor.Dark);
            Cells[4, 0].Piece = new King(4, 0, PieceColor.Dark);
            Cells[5, 0].Piece = new Bishop(5, 0, PieceColor.Dark);
            Cells[6, 0].Piece = new Night(6, 0, PieceColor.Dark);
            Cells[7, 0].Piece = new Rook(7, 0, PieceColor.Dark);

            Cells[0, 7].Piece = new Rook(0, 7, PieceColor.Light);
            Cells[1, 7].Piece = new Night(1, 7, PieceColor.Light);
            Cells[2, 7].Piece = new Bishop(2, 7, PieceColor.Light);
            Cells[3, 7].Piece = new Queen(3, 7, PieceColor.Light);
            Cells[4, 7].Piece = new King(4, 7, PieceColor.Light);
            Cells[5, 7].Piece = new Bishop(5, 7, PieceColor.Light);
            Cells[6, 7].Piece = new Night(6, 7, PieceColor.Light);
            Cells[7, 7].Piece = new Rook(7, 7, PieceColor.Light);
        }

        public void MakeMove(Move move)
        {
            SetPieceTo(move.Piece, move.ToX, move.ToY);
        }

        private void SetPieceTo(Piece piece, int x, int y)
        {
            var oldCell = Cells[piece.X, piece.Y];

            piece.MoveTo(x, y);

            Cells[x, y].Piece = piece;
            oldCell.Piece = null;
        }

        public IList<Move> AllMoves(PieceColor color)
        {
            var moves = new List<Move>();

            foreach (var cell in Cells)
            {
                if (cell.Piece != null &&
                    cell.Piece.Color == color)
                {
                    if (cell.Piece.Type == PieceType.King)
                    {
                        var kMoves = cell.Piece.GetMoves(Cells, true);
                        if (kMoves.Count > 0 && kMoves[0].ToX == -1)
                        {
                            return kMoves;
                        }
                        else
                        {
                            moves.AddRange(kMoves);
                        }
                    }
                    else
                    {
                        moves.AddRange(cell.Piece.GetMoves(Cells));
                    }
                }
            }

            return moves;
        }

        public string ValidateMove(MoveRecord move)
        {
            var piece = Cells[move.FromFile, move.FromRank].Piece;
            var currentColor = piece.Color;
            var toX = move.ToFile;
            var toY = move.ToRank;

            Piece king = null;

            foreach (var cell in Cells)
            {
                if (cell.Piece != null &&
                    cell.Piece.Type == PieceType.King &&
                    cell.Piece.Color == currentColor)
                {
                    king = cell.Piece;
                }
            }

            if (king == null)
            {
                return MoveValidationResult.KINGNOTFOUND;
            }

            var kingMoves = king.GetMoves(Cells, true);

            if (kingMoves.Count > 0 && kingMoves[0].ToX == -1 && kingMoves[0].ToY == -1)
            {
                if (piece.Type == PieceType.King)
                {
                    if (kingMoves.Exists(m => m.ToX == toX && m.ToY == toY))
                    {
                        if (Cells[toX, toY].Piece != null &&
                            Cells[toX, toY].Piece.Type == PieceType.King)
                        {
                            return MoveValidationResult.CONFIRM_AS_WIN;
                        }
                        else
                        {
                            return MoveValidationResult.CONFIRM;
                        }
                    }
                    else
                    {
                        return MoveValidationResult.DENY;
                    }
                }
                else
                {
                    return MoveValidationResult.DENY;
                }
            }
            else
            {
                if (piece.GetMoves(Cells).Exists(m => m.ToX == toX && m.ToY == toY))
                {
                    if (Cells[toX, toY].Piece != null &&
                        Cells[toX, toY].Piece.Type == PieceType.King)
                    {
                        return MoveValidationResult.CONFIRM_AS_WIN;
                    }
                    else
                    {
                        return MoveValidationResult.CONFIRM;
                    }
                }
                else
                {
                    return MoveValidationResult.DENY;
                }
            }
        }
    }
}
