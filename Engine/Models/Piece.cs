﻿using System.Collections.Generic;

namespace Engine.Models
{
    public abstract class Piece
    {
        public int X { get; set; }
        public int Y { get; set; }
        public PieceType Type { get; set; }
        public PieceColor Color { get; set; }
        public bool HasMoved { get; set; }

        public abstract List<Move> GetMoves(Cell[,] cells, bool top = false);

        public Piece(int x, int y, PieceType pieceType, PieceColor pieceColor)
        {
            X = x;
            Y = y;
            Type = pieceType;
            Color = pieceColor;
            HasMoved = false;
        }

        public void MoveTo(int x, int y)
        {
            X = x;
            Y = y;

            if (!HasMoved)
            {
                HasMoved = true;
            }
        }
    }
}
