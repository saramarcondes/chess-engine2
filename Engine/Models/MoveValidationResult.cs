﻿namespace Engine.Models
{
    public struct MoveValidationResult
    {
        public const string KINGNOTFOUND = "KING_NOT_FOUND";
        public const string CONFIRM = "CONFIRM";
        public const string DENY = "DENY";
        public const string CONFIRM_AS_WIN = "CONFIRM_AS_WIN";
    }
}
