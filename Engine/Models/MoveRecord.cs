﻿namespace Engine.Models
{
    public class MoveRecord
    {
        public int Id { get; set; }
        public int FromFile { get; set; }
        public int FromRank { get; set; }
        public int ToFile { get; set; }
        public int ToRank { get; set; }
    }
}
