﻿using Engine.Models;
using System;
using System.Text;

namespace Engine
{
    public class FenParser
    {
        public static string Create(Cell[,] cells)
        {
            var str = new StringBuilder();
            var empty = 0;

            for (int f = 0; f < 8; f++)
            {
                empty = 0;
                for (int r = 0; r < 8; r++)
                {
                    if (cells[r, f].Piece != null)
                    {
                        if (empty > 0)
                        {
                            str.Append(empty);
                            empty = 0;
                        }

                        str.Append(ParsePiece(cells[r, f].Piece));
                    }
                    else
                        empty++;
                }

                if (f != 7)
                {
                    if (empty > 0)
                    {
                        str.Append(empty);
                        empty = 0;
                    }

                    str.Append('/');
                }
            }

            return str.ToString();
        }

        public static Board ParsePieces(string line)
        {
            var ranks = line.Split('/');

            if (ranks.Length != 8)
            {
                throw new ArgumentException("Invalid FEN piece notation. Must have 8 ranks. ", "line");
            }

            var board = new Board();
            for (int r = 0; r < ranks.Length; r++)
            {
                var pieces = ranks[r];

                pieces.ToCharArray();

                if (pieces.Length == 8)
                {
                    for (int f = 0; f < 8; f++)
                    {
                        var pieceCharacter = pieces[f];
                        if (pieceCharacter == '1')
                        {
                            continue;
                        }
                        else
                        {
                            var p = PieceFactory.Create(pieceCharacter, f, r);
                            board.Cells[p.X, p.Y].Piece = p;
                        }
                    }
                }
                else
                {
                    var length = pieces.Length;
                    var counter = 0;
                    for (int f = 0; f < 8; f++)
                    {
                        var pieceCharacter = pieces[counter];
                        
                        switch (pieceCharacter)
                        {
                            case '1':
                            case '2':
                            case '3':
                            case '4':
                            case '5':
                            case '6':
                            case '7':
                            case '8':
                                f += int.Parse(pieceCharacter.ToString()) - 1;
                                break;
                            default:
                                var p = PieceFactory.Create(pieceCharacter, f, r);
                                board.Cells[p.X, p.Y].Piece = p;
                                break;
                        }

                        counter++;
                    }
                }

                //var empty = 0;
                //for (int f = 0; f < pieces.Length; f++)
                //{
                //    var pieceCharacter = pieces[f];
                //    if (pieceCharacter > 48 && pieceCharacter < 57)
                //    {
                //        if (int.Parse(pieceCharacter.ToString()) != 1)
                //            empty = int.Parse(pieceCharacter.ToString());
                //        else
                //            empty = 0;
                //        continue;
                //    }
                //    var p = PieceFactory.Create(pieceCharacter, f + empty, r);
                //    board.Cells[p.X, p.Y].Piece = p;
                //    empty = 0;
                //}
            }

            return board;
        }

        public static char ParsePiece(Piece piece)
        {
            if (piece.Color == PieceColor.Dark)
            {
                switch (piece.Type)
                {
                    case PieceType.Rook:
                        return 'r';
                    case PieceType.Night:
                        return 'n';
                    case PieceType.Bishop:
                        return 'b';
                    case PieceType.Queen:
                        return 'q';
                    case PieceType.King:
                        return 'k';
                    case PieceType.Pawn:
                        return 'p';
                    default:
                        throw new FormatException("Invalid PieceType");
                }
            }
            else
            {
                switch (piece.Type)
                {
                    case PieceType.Rook:
                        return 'R';
                    case PieceType.Night:
                        return 'N';
                    case PieceType.Bishop:
                        return 'B';
                    case PieceType.Queen:
                        return 'Q';
                    case PieceType.King:
                        return 'K';
                    case PieceType.Pawn:
                        return 'P';
                    default:
                        throw new FormatException("Invalid PieceType. ");
                }
            }
        }
    }
}
